package com.scb.pojos;

import lombok.Builder;
import lombok.Data;

@Builder(setterPrefix = "set")
public @Data class GetCasaTransactionsPojo {
    private String loginId;
    private long accountNo;
}
