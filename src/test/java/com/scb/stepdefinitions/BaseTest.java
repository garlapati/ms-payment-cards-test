package com.scb.stepdefinitions;

import com.gkc.enums.Method;
import com.gkc.headers.Headers;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.http.ContentType;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;

import java.io.IOException;

import static io.restassured.RestAssured.given;

public class BaseTest {

    protected Response response;
    private Scenario scenario;
    protected RequestSpecBuilder builder = new RequestSpecBuilder();

    @Before
    public void init(Scenario scenario){
        this.scenario = scenario;
    }

    public Response callAPIRequest(RequestSpecification requestSpecification, String URL, Method method){
        RestAssured.useRelaxedHTTPSValidation();
        response = given()
                .spec(requestSpecification)
                .log()
                .all()
                .when()
                .request(String.valueOf(method), URL)
                .then()
                .log()
                .all()
                .extract().response();

        return response;
    }

    public void addHeaders() throws IOException {
        builder.addHeaders(Headers.getHeaders());
        builder.addHeader("API_KEY", "Basic ajdkfkalsjdflkasdfj");
        builder.setContentType(ContentType.JSON);
    }
}
