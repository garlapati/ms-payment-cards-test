package com.scb.stepdefinitions;

import com.gkc.constants.FrameWorkConstants;
import com.gkc.headers.Headers;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import java.io.IOException;

public class MSPaymentCards {

    private final BaseTest base;

    public MSPaymentCards(BaseTest base){
        this.base = base;
    }

    @Given("the request with all valid headers")
    public void theRequestWithAllValidHeaders() throws IOException {
        base.addHeaders();
    }

    @When("post the request to {string}")
    public void postTheRequestTo(String path) {
        base.builder.addHeader("x-acc-jwt", Headers.getJwt());
        String URL = FrameWorkConstants.getMsPaymentCardRequest()+path;



    }

    @Then("validate the response status is {int}")
    public void validateTheResponseStatusIs(int arg0) {
    }

    @And("verify the response {string} as {string}")
    public void verifyTheResponseAs(String arg0, String arg1) {
    }

    @And("verify the respsone credit card number is {string} and debit card is {string}")
    public void verifyTheRespsoneCreditCardNumberIsAndDebitCardIs(String arg0, String arg1) {
    }

    @And("verify the response has {string} is {string} for {string}CC\"")
    public void verifyTheResponseHasIsForCC(String arg0, String arg1, String arg2) throws Throwable {    // Write code here that turns the phrase above into concrete actions    throw new cucumber.api.PendingException();}
    }
}
