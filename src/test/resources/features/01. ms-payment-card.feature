Feature: Credit and Debit Card Retrieval

  Scenario Outline: TC01_Verify the repsonse of the credit debit card retrieval
    Given the request with all valid headers
    When post the request to "<service>"
    Then validate the response status is 200
    And verify the response "status" as "SUCCESS"
    And verify the respsone credit card number is "452419894590949" and debit card is "3843843434353434"
    And verify the response has "productCode" is "MXFRCV" for "productType "CC"
    Examples:
      | service |
      |/digital/api/payment-cards-franksg/v1/payment-cards/consumer/credit-debit-card/retrival|
