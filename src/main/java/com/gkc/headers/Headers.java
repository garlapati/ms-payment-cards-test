package com.gkc.headers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.gkc.constants.FrameWorkConstants;
import lombok.Getter;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.UUID;

public final class Headers {

    private Headers(){}

    public static ObjectMapper mapper = new ObjectMapper();
    private static Map headers;

    public static Map<String, String> getHeaders() throws IOException{
        headers = mapper.readValue(new File(FrameWorkConstants.getCommonHeaders()), Map.class);
        headers.replace("x-correlation-id", String.valueOf(UUID.randomUUID()));
        return headers;
    }

    @Getter public static String jwt =
            "kdljfakldfjkladsfjadslkfjasdklfjsadklfjd";
}
