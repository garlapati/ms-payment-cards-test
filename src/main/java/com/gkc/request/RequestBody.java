package com.gkc.request;

import com.google.gson.Gson;
import com.google.gson.JsonObject;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

public class RequestBody {

    private RequestBody(){}
    private static Gson gson = new Gson();

    public static JsonObject getRequestBody(String path) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(new File(path));
        String file = IOUtils.toString(fileInputStream, "UTF-8");
        JsonObject requestBody = gson.fromJson(file, JsonObject.class);
        return requestBody;
    }
}
