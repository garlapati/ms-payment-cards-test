package com.gkc.constants;

import lombok.Getter;

public class FrameWorkConstants {

    private FrameWorkConstants(){}

    @Getter private static final String configPath = "src/test/resources/config.properties";
    @Getter private static final String commonHeaders = "src/test/resources/headers/commonHeaders.json";
    @Getter private static final String authorizeRequest = "src/test/resources/requests/authorize.json";
    @Getter private static final String instrospectRequest = "src/test/resources/requests/introspect.json";
    @Getter private static final String msPaymentCardRequest = "https://mcp-web.franksg.uat.gkc.com";


}
