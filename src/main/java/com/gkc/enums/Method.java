package com.gkc.enums;

public enum Method {
    GET,
    POST,
    DELETE,
    PUT
}
